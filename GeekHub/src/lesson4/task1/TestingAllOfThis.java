package lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class TestingAllOfThis {
    public static void main(String[] args){

        Implementation implementation = new Implementation();

        Set<Integer> first = new HashSet<>();
        Set<Integer> second = new HashSet<>();
        Set<Integer> third = new HashSet<>();

        first.add(1);
        first.add(2);
        first.add(3);
        first.add(4);
        first.add(5);
        first.add(6);

        second.add(5);
        second.add(6);
        second.add(7);
        second.add(8);
        second.add(9);
        second.add(10);

        System.out.println("A = " + first);
        System.out.println("B = " + second);
        System.out.println();

        System.out.println("first = second ? " + implementation.equals(first, second));

        third.addAll(implementation.union(first, second));
        System.out.println("union = " + third);
        third.clear();

        third.addAll(implementation.subtract(first, second));
        System.out.println("subtract = AB " + third);
        third.clear();

        third.addAll(implementation.subtract(second, first));
        System.out.println("subtract = BA " + third);
        third.clear();

        third.addAll(implementation.intersect(first, second));
        System.out.println("intersect = " + third);
        third.clear();

        third.addAll(implementation.symmetricSubtract(first, second));
        System.out.println("symmetric subtract " + third);
        third.clear();
    }
}
