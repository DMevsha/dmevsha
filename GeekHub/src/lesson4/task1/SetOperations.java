package lesson4.task1;


import java.util.Set;

public interface SetOperations {
    public boolean equals(Set A, Set B);
    public Set union(Set A, Set B);
    public Set subtract(Set A, Set B);
    public Set intersect(Set A, Set B);
    public Set symmetricSubtract(Set A, Set B);
}
