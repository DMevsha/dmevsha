package lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class Implementation implements SetOperations {
    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set c = new HashSet();
        Set d = new HashSet();
        c.addAll(a);
        d.addAll(b);
        c.addAll(d);

        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {

        Set c = new HashSet();
        Set d = new HashSet();
        c.addAll(a);
        d.addAll(b);
        c.removeAll(d);

        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set c = new HashSet();
        Set d = new HashSet();
        c.addAll(a);
        d.addAll(b);
        c.retainAll(d);

        return c;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a, b), subtract(b, a));
    }
}
