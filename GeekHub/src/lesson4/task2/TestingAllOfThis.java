package lesson4.task2;

import java.util.Date;

public class TestingAllOfThis {
    public static void main(String[] args){

        Reminder r = new Reminder();
        r.addTask(new Date(), new Task("1", "1"));
        r.addTask(new Date(100), new Task("2", "2"));
        r.addTask(new Date(10000000), new Task("3", "3"));
        r.addTask(new Date(1000000000), new Task("4", "4"));
        r.addTask(new Date(2000000000), new Task("5", "5"));
        r.addTask(new Date(300000000), new Task("1", "6"));
        r.addTask(new Date(20000), new Task("2", "7"));
        r.addTask(new Date(1500000000), new Task("1", "8"));

        System.out.println("Категории");
        System.out.println(r.getCategories());
        System.out.println("Задания по категориаям");
        System.out.println(r.getTasksByCategories());
        System.out.println("вторая категория");
        System.out.println(r.getTasksByCategory("2"));
        System.out.println("задачи на сегодня");
        System.out.println(r.getTasksForToday());

    }
}
