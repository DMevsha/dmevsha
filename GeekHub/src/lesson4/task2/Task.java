package lesson4.task2;

public class Task {
    String description;
    String category;

    public Task(String category, String description){
        this.category = category;
        this.description = description;

    }
    public String toString(){
        return "\nCategory: " + category + "\ndescription: " + description;
    }
}
