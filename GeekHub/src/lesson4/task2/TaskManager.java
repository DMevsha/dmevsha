package lesson4.task2;

import java.util.*;

public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date);
    public Collection<String> getCategories();
    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();

}
