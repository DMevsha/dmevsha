package lesson4.task2;

import java.util.*;

public class Reminder implements TaskManager{
    //the field keeps all tasks
    Map<Date, Task> taskList = new TreeMap<>();

    @Override
    public void addTask(Date date, Task task) {
        taskList.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        taskList.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new TreeSet<>();

        //content of map correlated with presentation of map as a set
        Set<Map.Entry<Date, Task>> set = taskList.entrySet();

        for (Map.Entry<Date, Task> me : set){
            categories.add(me.getValue().category);
        }

        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tm = new TreeMap<>();

        //collection keeps the list of all categories
        Collection<String> categories = this.getCategories();
        List<Task> temporaryTasks;


        for (String category : categories){

            //we save tasks on categories
            temporaryTasks = this.getTasksByCategory(category);
            tm.put(category, temporaryTasks);

        }
        return tm;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasks = new ArrayList<>();

        //content of map correlated with presentation of map as a set
        Set<Map.Entry<Date, Task>> set = taskList.entrySet();
        for (Map.Entry<Date, Task> me : set){

            //we choose tasks proper the set category
            if (me.getValue().category.equals(category)){
            tasks.add(me.getValue());
            }
        }
        return tasks;
    }

    @Override
    public List<Task> getTasksForToday() {
        Date today = new Date();
        List<Task> tasks = new ArrayList<>();

        //content of map correlated with presentation of map as a set
        Set<Map.Entry<Date, Task>> set = taskList.entrySet();
        for (Map.Entry<Date, Task> me : set){

            //we choose tasks planned for today
            if (this.sameDay(me.getKey().getTime(), today.getTime())){
                tasks.add(me.getValue());
            }
        }
        return tasks;
    }
    private boolean sameDay(long firstTimeInMillis, long secondTimeInMillis){

        //an amount of milliseconds is in day
        int millis_in_day = 1000 * 60 * 60 * 24;

        //complete days from the beginning of calculation (01.01.1970)
        int number_of_first_day = (int) (firstTimeInMillis / millis_in_day);
        int number_of_second_day = (int)(secondTimeInMillis / millis_in_day);

        //if numbers are equal it means a that day
        return (number_of_first_day == number_of_second_day);
    }
}
