package lesson8;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster.
 * To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
        createNewFolderIfNotExist(folder);
    }

    /**
     * Call this method to start download images from specified URL.
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        HashSet<URL> images = (HashSet<URL>) page.getImageLinks();
        for (URL image : images){
            if (isImageURL(image)){
                executorService.execute( new ImageTask( image, folder ) );
            }
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        String u = url.toString();
        return (u.contains(".jpg") || u.contains(".git") || u.contains(".png"));
    }

    private void createNewFolderIfNotExist(String path){
        Path folder = new File(path).toPath();
        if(Files.notExists(folder)){
            try {
                System.out.println("Folder named as: " + path + " not exist");
                Files.createDirectory(folder);
                System.out.println(path + " created");

            } catch (IOException e) {
                System.out.println("creating folder failure application will be closed");
                System.exit(0);
            }
        }
    }



}
