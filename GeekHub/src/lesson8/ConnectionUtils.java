package lesson8;

import java.io.*;
import java.net.*;
import java.net.URLConnection;
import java.nio.ByteBuffer;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {

        byte[] bytes;
        byte[] buffer = new byte[2048];
        URLConnection connection = url.openConnection();

        try (InputStream inputStream = connection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream()){

            int len;

            while ((len = bufferedInputStream.read(buffer)) != -1){
            byteArray.write(buffer, 0, len);
            }
            bytes = byteArray.toByteArray();

        }
        return bytes;
    }
}
