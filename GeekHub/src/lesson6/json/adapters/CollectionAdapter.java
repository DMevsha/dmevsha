package lesson6.json.adapters;

import lesson6.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {


    @Override
    public Object toJson(Collection c) {
        JSONArray jsonArray = new JSONArray();
        for (Object o : c){
            jsonArray.put(JsonSerializer.serialize(o));
        }
        return jsonArray;
    }
}
