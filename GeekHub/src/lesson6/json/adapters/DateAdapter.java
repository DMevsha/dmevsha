package lesson6.json.adapters;

import java.util.Calendar;
import java.util.Date;

/**
 * Converts object of type java.util.Date to String by using dd/MM/yyyy format
 */
public class DateAdapter implements JsonDataAdapter<Date> {
    @Override
    public Object toJson(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return "(" + calendar.get(Calendar.DAY_OF_MONTH) + "/" +
                     calendar.get(Calendar.MONTH) + "/" +
                     calendar.get(Calendar.YEAR) + ")";
    }
}
