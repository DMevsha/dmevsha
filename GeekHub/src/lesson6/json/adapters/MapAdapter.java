package lesson6.json.adapters;

import org.json.JSONException;
import org.json.JSONObject;
import lesson6.json.JsonSerializer;

import java.util.Map;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */

public class MapAdapter implements JsonDataAdapter<Map> {
    @Override
    public Object toJson(Map map) {
        JSONObject m = new JSONObject();
        for (Object key : map.keySet()) {
            try {
                m.put(key.toString(), JsonSerializer.serialize(map.get(key)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return m;
    }
}
