package lesson6.json.adapters;

/**
 * JsonDataAdapter contains instructions how to serialize Java object to Json representation.
 * @param <T> determines type adapter works with.
 */
public interface JsonDataAdapter<T> {
   public Object toJson(T o);
}
