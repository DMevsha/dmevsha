package lesson3.task1;

public class TestingAllOfThis {
    public static void main(String[] args){

        Book firstBook =   new Book(1900, "Smith");
        Book secondBook =  new Book(1905, "Garry");
        Book thirdBook =   new Book(1190, "Barry");
        Book fourthBook =  new Book(1901, "Smith");
        Book fifthBook =   new Book(1900, "Barry");
        Book sixthBook =   new Book(2012, "Lewis");
        Book seventhBook = new Book(1900, "Smith");
        Book eighthBook =  new Book(2013, "Terry");
        Book ninthBook =   new Book(2015, "Marry");
        Book tenthBook =   new Book(1945, "Smith");

        Book[] allBooks = {firstBook, secondBook, thirdBook, fourthBook, fifthBook,
                sixthBook, seventhBook, eighthBook, ninthBook, tenthBook};

        System.out.println("\nAllBooks Before Sort\n");
        for (Book b: allBooks){
            System.out.println(b);
        }

        System.out.println("\nAfter Sort\n");

        ArrayComparator AC = new ArrayComparator();

        Comparable[] sortedBooks = AC.sort(allBooks);
        for (Comparable b: sortedBooks){
            System.out.println(b);
        }

        System.out.println("\nAll Books After Sort\n");
        for (Book b: allBooks){
            System.out.println(b);
        }

    }
}
