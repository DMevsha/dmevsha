package lesson3.task1;

public class Book implements Comparable<Book>{

    //book was published in this year
    private int published;

    //author of this book
    private String author;

    public Book(int year, String author){
        published = year;
        this.author = author;
    }
    @Override
    //at first we compare on an author, if at a book same author that on the year of published
    public int compareTo(Book book) {

        if (author.equals(book.author)){
           return published - book.published;
        }
        else {
            return author.compareTo(book.author);
        }
    }

    public String toString(){
        return "author " + author + " was published in " + published;
    }

}
