package lesson3.task1;

public class ArrayComparator {

    public Comparable[] sort(Comparable[] elements){
        Comparable[] temporary = elements.clone();
        Comparable comparable;
        int count = 1;
        while (count > 0){
            count = 0;
            for (int i = 0; i < temporary.length - 1; i++){
                if (temporary[i].compareTo(temporary[i+1]) > 0){
                    comparable  =  temporary[i];
                    temporary[i] = temporary[i+1];
                    temporary[i+1] = comparable;
                    count++;
                }
            }
        }
        return temporary;
    }
}
