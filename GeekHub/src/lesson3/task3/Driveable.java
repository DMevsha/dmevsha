package lesson3.task3;

public interface Driveable {
    public void accelerate() throws OverspeedException;
    public void brake();
    public void turn();
    public void ride() throws NotEnoughFuelsException;
    public void addFuel() throws OverLimitException;
    public void fuelLeft();
}
