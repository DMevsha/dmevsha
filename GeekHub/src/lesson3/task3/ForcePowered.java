package lesson3.task3;

//basic class for the sources of force
abstract class ForcePowered {
    protected double capacity;
    protected double amount;

    public double getAmount(){
        return amount;
    }
    public void setAmount(double amount){
        this.amount  = amount;
    }
    boolean WeCanRide(double expense){
        if (amount < expense){
            return false;
        }else{
            return true;
        }
    }

    double getCapacity() {
        return capacity;
    }

    boolean WeCanAddFuel(double amount){
        if ((this.amount + amount) > capacity){

            return false;
        }else{
            return true;
        }
    }
}
