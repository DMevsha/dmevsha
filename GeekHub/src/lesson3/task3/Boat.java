package lesson3.task3;

public class Boat extends Vehicle {
    String name;
    Engine engine = new Engine("Diesel", 20);
    GasTank gas_tank;
    String direction;
    double speed;

    public Boat(String name, int amount_of_fuel){
        this.name = name;
        gas_tank = new GasTank(amount_of_fuel, 300);
        maximum_speed = 50;
        mover = new Screw();
    }

   @Override
    public void turn() {
        direction = "New Direction";
        System.out.println("курс лодки изменен");
    }

    @Override
    public void ride() throws NotEnoughFuelsException {
        if (gas_tank.WeCanRide(engine.getEXPENSE_OF_FUEL())){
            System.out.println("100 КМ проплыто");
            gas_tank.setAmount(gas_tank.getAmount() - engine.getEXPENSE_OF_FUEL());
            distance +=100;
        }else{
            throw new NotEnoughFuelsException();
        }
    }

    @Override
    public void addFuel() throws OverLimitException {
        if (gas_tank.WeCanAddFuel(10)){
            gas_tank.setAmount(gas_tank.getAmount() + 10);
            System.out.println("10 литров топлива влито в бак");
        }else {
            throw new OverLimitException();
        }
    }

    @Override
    public void fuelLeft() {
        System.out.println("В баке осталось: " + gas_tank.getAmount() + " литров топлива");
    }

    @Override
    public void accelerate() throws OverspeedException {
        engine.accelerate();

        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        mover.setSpeed(engine.getTurns_per_minutes() / 10 );

        speed = mover.getSpeed();

        System.out.println("скорость лодки теперь составляет " + speed + " км/час");
        if (speed > maximum_speed){
            throw new OverspeedException();
        }
    }

    @Override
    public void brake() {
        engine.brake();
        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        mover.setSpeed(engine.getTurns_per_minutes() / 10 );

        speed = mover.getSpeed();
        System.out.println("скорость лодки теперь составляет " + speed + " км/час");
    }

    @Override
    public String toString(){
        return "Название: " + name +
                "\nТип двигателя: " + engine.getType() +
                "\nУровень топлива: " + gas_tank.getAmount() +
                "\nМаксимальный уровень топлива: " + gas_tank.getCapacity() +
                "\nСкорость: " + speed +
                "\nОграничение скорости: " + maximum_speed +
                "\nВсего проехано: " + distance;
    }

}
