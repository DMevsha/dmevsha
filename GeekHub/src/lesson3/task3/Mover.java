package lesson3.task3;

public abstract class Mover {
    double speed;

    double getSpeed() {
        return speed;
    }

    void setSpeed(double speed) {
        this.speed = speed;
    }
}
