package lesson3.task3;

public class SolarPoweredCar extends Vehicle {

    public SolarPoweredCar(String name, double charging_level){
        this.name = name;
        gas_tank = new SolarBattery(charging_level);
        engine = new Engine("Solar powered", 25);
        mover = new Wheels();
        maximum_speed = 110;
    }

    @Override
    public void turn() {
        System.out.println("Направление движения Е-Мобиля изменено");
    }

    @Override
    public void ride() throws NotEnoughFuelsException {
        if (gas_tank.WeCanRide(engine.getEXPENSE_OF_FUEL())){
            System.out.println("100 КМ проехано");
            gas_tank.setAmount(gas_tank.getAmount() - engine.getEXPENSE_OF_FUEL());
            distance +=100;
        }else{
            throw new NotEnoughFuelsException();
        }
    }

    @Override
    public void addFuel() throws OverLimitException {
        if (gas_tank.WeCanAddFuel(10)){
            gas_tank.setAmount(gas_tank.getAmount() + 10);
            System.out.println("батараея успешно была заряжена на 10%");
        }else {
            throw new OverLimitException();
        }
    }

    @Override
    public void fuelLeft() {
        System.out.println("В батарее осталось: " + gas_tank.getAmount() + "% зарядки");
    }

    @Override
    public void accelerate() throws OverspeedException {
        engine.accelerate();

        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        mover.setSpeed(engine.getTurns_per_minutes() / 10 );

        speed = mover.getSpeed();

        System.out.println("скорость Е-мобиля теперь составляет " + speed + " км/час");
        if (speed > maximum_speed){
            throw new OverspeedException();
        }
    }

    @Override
    public void brake() {
        engine.brake();
        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        mover.setSpeed(engine.getTurns_per_minutes() / 10 );

        speed = mover.getSpeed();
        System.out.println("скорость Е-мобиля теперь составляет " + speed + " км/час");
    }

}
