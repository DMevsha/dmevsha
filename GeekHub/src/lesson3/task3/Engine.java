package lesson3.task3;

public class Engine {
    //expenses of fuel for 100 KM
    final double EXPENSE_OF_FUEL;

    public String getType() {
        return type;
    }

    String type;
    private double turns_per_minutes = 0;

    double getEXPENSE_OF_FUEL() {
        return EXPENSE_OF_FUEL;
    }

    Engine (String type, double expense){
        this.type = type;
        EXPENSE_OF_FUEL = expense;

    }

    void accelerate(){
        //grow the turns of engine
        turns_per_minutes += 100;
    }

    void brake(){
        //decline of turns of engine
        turns_per_minutes -= 100;
    }

    double getTurns_per_minutes() {
        return turns_per_minutes;
    }

}