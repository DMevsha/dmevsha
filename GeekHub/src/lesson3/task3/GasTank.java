package lesson3.task3;

public class GasTank extends ForcePowered{
    //create new Gas-Tank with any amount of fuel in litres
    GasTank(double amount_of_fuel, double capacity){
        amount = amount_of_fuel;
        this.capacity = capacity;
    }
}
