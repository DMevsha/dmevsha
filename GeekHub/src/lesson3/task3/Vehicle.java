package lesson3.task3;

public abstract class Vehicle implements Driveable {
    Mover mover;
    Engine engine;
    ForcePowered gas_tank;
    double speed;
    String name;
    double maximum_speed;
    int distance = 0;

    public String toString(){
        return "Название: " + name +
               "\nТип двигателя: " + engine.getType() +
               "\nУровень топлива: " + gas_tank.getAmount() +
               "\nМаксимальный уровень топлива: " + gas_tank.getCapacity() +
               "\nСкорость: " + speed +
               "\nОграничение скорости: " + maximum_speed +
               "\nВсего проехано: " + distance;
    }
}
