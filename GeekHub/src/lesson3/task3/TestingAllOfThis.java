package lesson3.task3;

import java.util.Scanner;

public class TestingAllOfThis {

    static String help(){
        return "сначала выберите транспортное средством которым Вы хотите управлять, введите:"+
                "\nic - Инжекторная машина"+"\nec - Е-Мобиль"+"\nb - Лодка";
    }
    static String methods(){
        return  "команды применимы к транспортному средству" +
                "\nstatus - увидеть информацию по состоянию выбраного транспортного средства" +
                "\ngo - проехать 100 км" +
                "\naccelerate - повысить скорость на 10 км/час" +
                "\nbrake - снизить скорость на 10 км/час" +
                "\nfueling - увеличить уровень топлива на 10" +
                "\nfuelleft - узнать уровень топлива" +
                "\nтак же Вы можите сменить транспортное средство" +
                "\nic - Инжекторная машина"+"\nec - Е-Мобиль"+"\nb - Лодка";
    }

    public static void main(String [] args){
        SolarPoweredCar mySolarCar = new SolarPoweredCar("E-Mobile", 79);
        InjectorCar myInjectorCar = new InjectorCar("Porsche", 50);
        Boat myBoat = new Boat("Titanic", 300);
        Vehicle vehicle = null;
        boolean unselected = true;
        boolean running = true;
        Scanner scanner = new Scanner(System.in);
        String val;
        while (unselected){
            System.out.println("для справки введите символ \"?\" или help. Введите stop для закрытия программы");
            val = scanner.nextLine();
            switch(val){
                case "?":
                case "help":
                    System.out.println(help());
                    break;
                case "stop":
                    running = false;
                    unselected = false;
                    break;
                case "ic":
                    vehicle = myInjectorCar;
                    unselected = false;
                    break;
                case "ec":
                    vehicle = mySolarCar;
                    unselected = false;
                    break;
                case "b":
                    vehicle = myBoat;
                    unselected = false;
                    break;
            }
        }
        while (running){
            System.out.println("для справки введите символ \"?\" или help. Введите stop для закрытия программы");
            val = scanner.nextLine();
            switch(val){
                case "?":
                case "help":
                    System.out.println(methods());
                    break;
                case "stop":
                    running = false;
                    break;
                case "status":
                    System.out.println(vehicle);
                    break;
                case "go":
                    try {
                        vehicle.ride();
                    } catch (NotEnoughFuelsException e) {
                        System.out.println(e);
                    }finally {
                        break;
                    }
                case "accelerate":
                    try {
                        vehicle.accelerate();
                    } catch (OverspeedException e) {
                        System.out.println(e);
                    }finally {
                        break;
                    }
                case "brake":
                    vehicle.brake();
                    break;
                case "fueling":
                    try {
                        vehicle.addFuel();
                    } catch (OverLimitException e) {
                        System.out.println(e);
                    }finally {
                        break;
                    }
                case "fuelleft":
                    vehicle.fuelLeft();
            }
        }
        scanner.close();
    }
}
