package lesson3.task2;

public class Testing {
    public static void main(String[] args){
        long timeBefore, timeAfter;
        String string = " ";
        StringBuilder stringBuilder = new StringBuilder(" ");
        StringBuffer stringBuffer = new StringBuffer(" ");
        int count = 10000;
        timeBefore = System.currentTimeMillis();
        for (int z = 0; z < count; z++){
            for (int i = 0; i < 26; i++){
            string += (char)('A' + i);
            }
        }
        timeAfter = System.currentTimeMillis();
        System.out.println(timeAfter - timeBefore);

        timeBefore = System.currentTimeMillis();
        for (int z = 0; z < count; z++){
            for (int i = 0; i < 26; i++){
                stringBuilder.append((char)('A' + i));
            }
        }
        timeAfter = System.currentTimeMillis();
        System.out.println(timeAfter - timeBefore);

        timeBefore = System.currentTimeMillis();
        for (int z = 0; z < count; z++){
            for (int i = 0; i < 26; i++){
                stringBuffer.append((char)('A' + i));
            }
        }
        timeAfter = System.currentTimeMillis();
        System.out.println(timeAfter - timeBefore);

    }
}
