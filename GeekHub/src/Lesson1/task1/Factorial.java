package Lesson1.task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Factorial {
    // a method is assured returns the number entered an user from 0 to 12
    private static int readConsole(){
        // we create object scanner type of Scanner, the constructor of which as an argument
        // accepts an incoming stream, at reading from a console it is a stream of System.in
        Scanner scanner = new Scanner(System.in);

        // info for user
        System.out.println("Ведите целое число от 0 до 12 для вычисления факториала");

        int myInt;

        try { // check keyboard input
            myInt = scanner.nextInt();
        } // if an user entered a not integer type catch that exception
        catch(InputMismatchException e){
            // use a recursion while an user will not enter an integer
            myInt = readConsole();
        }
         /* an exception was not, but remained to check whether the entered number
          * corresponds to the requirements, on determination a factorial is certain
          * for non-negative integers, but because a function increases very quickly
          * it is needed to take into account that factorial of number 13 will go
          * out outside a type int
         */
        while (myInt < 0 | myInt > 12){
            // again recursion...
            myInt = readConsole();
        }
        return myInt;
    }
    public static void main (String args[]){
        int result = 1;
        int myInt = readConsole();

        // factorial 0 and 1 is equal 1, there is not sense to find them
        for (int i = 2; i <= myInt; i++){
                result *= i;
        }// output result to the console
        System.out.println("факториал числа " + myInt + " будет равен: " + result);
    }
}
