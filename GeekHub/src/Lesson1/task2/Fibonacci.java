package Lesson1.task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Fibonacci {
    // a method is assured returns the number entered an user from 0 to 46
    private static int readConsole(){
        // we create object scanner type of Scanner, the constructor of which as an argument
        // accepts an incoming stream, at reading from a console it is a stream of System.in
        Scanner scanner = new Scanner(System.in);

        // info for user
        System.out.println("Ведите целое число от 0 до 46 для вычисления последовательности Фибоначчи");
        int myInt;

        try {   // check keyboard input
            myInt = scanner.nextInt();
        }    // if an user entered a not integer type catch that exception
        catch(InputMismatchException e){
            // use a recursion while an user will not enter an integer
            myInt = readConsole();
        }
         /* an exception was not, but remained to check whether the entered number
          * corresponds to the requirements, because a function increases very quickly
          * it is needed to take into account that factorial of number 46 will go
          * out outside a type int
         */

        while (myInt < 0 | myInt > 46){
            // again recursion...
            myInt = readConsole();
        }
        return myInt;
    }
    public static void main(String args[]){
        int myInt = readConsole();
        // a variable will be used for storage of sum for (myInt - 1) element of sequence
        int previous = 1;
        // a variable will be used foe storage of sum for (myInt - 2) element of sequence
        int prePrevious = -1;
        int temporary;

        System.out.println("последовательность Фибоначчи для первых " + myInt + " чисел:");

        for (int i = 0; i <= myInt; i++){
            System.out.println((previous + prePrevious));
            temporary = prePrevious;
            prePrevious = previous;
            previous = temporary + prePrevious;
        }
    }
}
