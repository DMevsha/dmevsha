package lesson2;

//basic class for the sources of force
abstract class ForcePowered {
    protected double amount;
    protected double consumption;

    void setConsumption(double consumption){
        this.consumption = consumption;
    }

    //how many complete hours will be work engine at this level of consumption
    int howLongWeCanRide(){
        return (int)(amount / consumption * 60);
    }
}
