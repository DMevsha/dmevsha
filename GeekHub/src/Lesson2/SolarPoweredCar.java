package lesson2;

public class SolarPoweredCar extends Vehicle {

    public SolarPoweredCar(String name, double charging_level){
        this.name = name;
        gas_tank = new SolarBattery(charging_level);
        engine = new Engine("Solar powered", 0.25);
        wheels = new Wheels(1.2);
        count_of_passengers = 2;
    }

    @Override
    public void turn() {
        wheels.setDirection("New Direction");
        System.out.println("Направление движения Е-Мобиля изменено");
    }

    @Override
    public void accelerate() {
        engine.accelerate();
        gas_tank.setConsumption(engine.getConsumption());

        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        wheels.setSpeed(engine.getTurns_per_minutes() / 17 );

        //translate meters per minute to kilometres per hour
        speed = wheels.getSpeed() * wheels.getSize() * 0.06 ;

        System.out.println("скорость Е-Мобиля теперь составляет " + speed +
                " км/час заряда батареи приблизительно хватит на " + gas_tank.howLongWeCanRide() + " часов" +
                " или " + speed * gas_tank.howLongWeCanRide() + " километров");
    }

    @Override
    public void brake() {
        engine.brake();
        gas_tank.setConsumption(0);
        wheels.setSpeed(0);
        speed = 0;
        System.out.println("Е-Мобиль остановлен");
    }

}
