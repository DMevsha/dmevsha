package lesson2;

public class Boat implements Driveable{
    String name;
    Engine engine = new Engine("Diesel", 0.3);
    Gas_Tank gas_tank;
    String direction;
    double speed;

    public Boat(String name, int amount_of_fuel){
        this.name = name;
        gas_tank = new Gas_Tank(amount_of_fuel);
    }

    @Override
    public void turn() {
        direction = "New Direction";
        System.out.println("курс лодки изменен");
    }

    public void accelerate() {
        engine.accelerate();
        gas_tank.setConsumption(engine.getConsumption());

        //due to a transmission, the amount of turns of engine differs from the amount of speed
        //and translate to kilometres per hour
        speed = (engine.getTurns_per_minutes() / 10 * 0.06 );

        System.out.println("скорость лодки в стоячей воде теперь составляет " + speed +
                " км/час топлива приблизительно хватит на " + gas_tank.howLongWeCanRide() + " часов" +
                " или " + speed * gas_tank.howLongWeCanRide() + " километров");
    }

    @Override
    public void brake() {
        engine.brake();
        gas_tank.setConsumption(0);
        speed = 0;
        System.out.println("лодка остановлена");
    }

}
