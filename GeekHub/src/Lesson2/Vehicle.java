package lesson2;

public abstract class Vehicle implements Driveable{
    Wheels wheels;
    Engine engine;
    ForcePowered gas_tank;
    double speed;
    String name;
    int count_of_passengers;
}
