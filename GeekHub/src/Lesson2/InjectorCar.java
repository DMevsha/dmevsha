package lesson2;

public class InjectorCar extends Vehicle {

    public InjectorCar(String name, double amount_of_fuel){
        this.name = name;
        gas_tank = new GasTank(amount_of_fuel);
        engine = new Engine("Injector", 0.09);
        wheels = new Wheels(1.5);
        count_of_passengers = 5;
    }

    @Override
    public void turn() {
        wheels.setDirection("New Direction");
        System.out.println("Направление движения инжекторной машины изменено");
    }

    @Override
    public void accelerate() {
        engine.accelerate();
        gas_tank.setConsumption(engine.getConsumption());

        //due to a transmission, the amount of turns of engine differs from the amount of turns of wheels
        wheels.setSpeed(engine.getTurns_per_minutes() / 5 );

        //translate meters per minute to kilometres per hour
        speed = wheels.getSpeed() * wheels.getSize() * 0.06 ;

        System.out.println("скорость инжекторной машины теперь составляет " + speed +
                           " км/час топлива приблизительно хватит на " + gas_tank.howLongWeCanRide() + " часов" +
                           " или " + speed * gas_tank.howLongWeCanRide() + " километров"  );
    }

    @Override
    public void brake() {
        engine.brake();
        gas_tank.setConsumption(0);
        wheels.setSpeed(0);
        speed = 0;
        System.out.println("Инжекторная машина остановлена");
    }

}
