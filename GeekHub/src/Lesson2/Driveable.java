package lesson2;

public interface Driveable {

    //change direction
    void turn();

    //increase speed
    void accelerate();

    //stop
    void brake();

}
