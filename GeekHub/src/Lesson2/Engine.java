package lesson2;

public class Engine {
    //expenses of fuel per one engines turn
    private final double EXPENSE_OF_FUEL;

    private String type;
    private double turns_per_minutes = 0;

    //expenses of fuel in a minute
    private double consumption;

    Engine (String type, double expense){
        this.type = type;
        EXPENSE_OF_FUEL = expense;
    }

    void accelerate(){
        //grow the turns of engine, that increases the amount of consumption of fuel also
        turns_per_minutes += 1000;
        setConsumption(EXPENSE_OF_FUEL * turns_per_minutes);
    }

    void brake(){
        //stop engine, that stops the amount of consumption of fuel also
        turns_per_minutes = 0;
        setConsumption(0);
    }

    double getTurns_per_minutes() {
        return turns_per_minutes;
    }

    double getConsumption() {
        return consumption;
    }

    private void setTurns_per_minutes(double turns_per_minutes) {
        this.turns_per_minutes = turns_per_minutes;
    }

    private double getEXPENSE_OF_FUEL(){
        return EXPENSE_OF_FUEL;
    }

    private void setConsumption(double consumption) {
        this.consumption = consumption;
    }

}