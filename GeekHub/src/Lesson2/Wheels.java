package lesson2;


public class Wheels {
    //amount of wheels turns per several time
    private double speed;

    String direction;

    //size of one wheel step
    private final double size;
    
    Wheels(double size){
        this.size = size;
    }

    double getSpeed() {
        return speed;
    }

    void setSpeed(double speed) {
        this.speed = speed;
    }

    void setDirection(String direction) {
        this.direction = direction;
    }

    double getSize() {
        return size;
    }
}
