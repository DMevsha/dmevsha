package lesson2;

import java.util.ArrayList;

public class TestAllOfThis {
    public static void main(String args[]){

        InjectorCar myInjectorCar = new InjectorCar("Porsche", 50);
        Boat myBoat = new Boat("Titanic", 300);
        SolarPoweredCar mySolarCar = new SolarPoweredCar("E-Mobile", 79);

        ArrayList<Driveable> myList = new ArrayList<Driveable>();
        myList.add(mySolarCar);
        myList.add(myInjectorCar);
        myList.add(myBoat);

        for (Driveable driveable : myList){
            driveable.accelerate();
            driveable.turn();
            driveable.accelerate();
            driveable.brake();
            System.out.println();
        }
}
}