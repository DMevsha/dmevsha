package lesson5;

import lesson5.source.URLSourceProvider;


import java.io.IOException;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at
 * <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */
public class Translator {
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at
     * <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY =
            "trnsl.1.1.20131114T144948Z.34c161ab120d561b.f952335566ab068d3d25d6e69218e82e431aa5fc";
    private static final String TRANSLATION_DIRECTION = "ru";

    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws java.io.IOException
     */
    public String translate(String original) throws IOException{

        StringBuilder sb = new StringBuilder();
        String separator = "[.]";

        if (original.contains(".")){
            for (String s : original.split(separator)){
                sb.append( parseContent( urlSourceProvider.load( prepareURL( s ) ) ) ).append(".");
            }
        }else {
            sb.append( parseContent( urlSourceProvider.load( prepareURL( original ) ) ) );
        }
        return sb.toString();

    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text
     */
    private String prepareURL(String text) {

        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + YANDEX_API_KEY
                + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service.
     * Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) {
        if (content.equals("CONNECTION FAIL")){
            return " <-- text is missed -->";
        }
        String s = content.substring(content.indexOf("<text>") + 6, content.indexOf("</text>"));
        s = s.replace("%%%", "\n");
        return s;
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) {
        return text.replace("\n", "%%%");
    }

}
