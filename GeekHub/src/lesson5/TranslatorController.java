package lesson5;

import lesson5.source.SourceLoader;
import lesson5.source.URLSourceProvider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException{
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        //String source = sourceLoader.loadSource("https://dl.dropboxusercontent.com/u/14434019/en.txt");
        //String translation = translator.translate(source);


        //System.out.println("Original: " + source);
        //System.out.println("Translation: " + translation);
        System.out.println("input absolute path to file, which you would like to translate, or input \"exit\" to stop");

        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();

         while(!"exit".equals(command)) {

            //So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);


            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);
            }catch (UnknownHostException e){
                System.out.println("unable to connect: " + command);

            }catch (FileNotFoundException e){
                System.out.println("file not found: " + command);
            }catch (IOException e){
                System.out.println(e.getMessage());
            }
            command = scanner.nextLine();
        }
    }
}
