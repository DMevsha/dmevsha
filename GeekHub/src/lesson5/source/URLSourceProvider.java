package lesson5.source;

import java.io.*;
import java.net.*;
import java.net.HttpURLConnection;


/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {

        return (pathToSource.indexOf("http://")  == 0 |
                pathToSource.indexOf("https://") == 0 |
                pathToSource.indexOf("ftp://")   == 0);
    }

    @Override
    public String load(String pathToSource) throws IOException{
        URL url = new URL(pathToSource);
        StringBuilder sb = new StringBuilder();
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        if (connection.getResponseCode() != 200){
            return "CONNECTION FAIL";
        }
        try(InputStreamReader input = new InputStreamReader(connection.getInputStream(), "UTF-8")){


        int c;
        while ((c = input.read()) != -1) {
            sb.append((char)c);
        }
        }catch (IOException e){
           System.out.println("corrupted stream");

        }

        return sb.toString();
    }
}
