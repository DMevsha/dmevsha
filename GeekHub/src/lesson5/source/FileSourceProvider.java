package lesson5.source;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {

        return (pathToSource.indexOf(":/") == 1);
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder sb = new StringBuilder();

        try(InputStreamReader input = new InputStreamReader(new FileInputStream(pathToSource), "UTF-8")){

            int c;
            while ((c = input.read()) != -1) {
                sb.append((char)c);
            }
        }catch (FileNotFoundException e){
            throw new FileNotFoundException();
        }
        catch (IOException e){
            System.out.println("corrupted stream");

        }

        return sb.toString();
    }

}
