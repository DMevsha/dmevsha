package lesson5.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        sourceProviders.add(new URLSourceProvider());
        sourceProviders.add(new FileSourceProvider());
    }
    //an application also will begin to translate any simple text from a console if to clean the marked lines
    public String loadSource(String pathToSource) throws IOException {
        String text = pathToSource;
        boolean changeless = true;                                          // <-- this
        for (SourceProvider sp : sourceProviders){
                if (sp.isAllowed(pathToSource)){
                    text = sp.load(pathToSource);
                    changeless = false;                                     //  <-- this
                }
        }
        if (changeless){                                                    //  <--
            throw new IOException("Bad path to file: " + pathToSource);     //  <--     and all of this
        }                                                                   //  <--
        return text;
    }
}
