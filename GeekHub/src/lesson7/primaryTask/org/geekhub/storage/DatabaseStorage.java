package lesson7.primaryTask.org.geekhub.storage;

import lesson7.primaryTask.org.geekhub.objects.Entity;
import lesson7.primaryTask.org.geekhub.objects.Ignore;
import lesson7.primaryTask.org.geekhub.objects.PrimaryClass;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link lesson7.primaryTask.org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link lesson7.primaryTask.org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from " + clazz.getSimpleName());
        List<T> list = extractResult(clazz, resultSet);
        statement.close();
        return list;
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        Statement statement = connection.createStatement();
        boolean success = statement.execute("delete from " + entity.getClass().getSimpleName() +
                                            " where id='" + entity.getId() + "'");
        statement.close();
        return success;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        StringBuilder stringBuilder = new StringBuilder();
        if (entity.isNew()) {
            int newId = entity.getNewId();
            entity.setId(newId);
            data.put("id", newId);
            stringBuilder.append("insert into ").append(entity.getClass().getSimpleName()).append(" (");
            for (String key : data.keySet()){
                stringBuilder.append(key).append(", ");
            }
            stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());

            stringBuilder.append(") values ('");

            for (String key : data.keySet()){
                stringBuilder.append(data.get(key)).append("', '");
            }
            stringBuilder.delete(stringBuilder.length() - 4, stringBuilder.length());

            stringBuilder.append("')");

            sql = stringBuilder.toString();

        } else {
            stringBuilder.append("update ").append(entity.getClass().getSimpleName()).append(" set ");
            for (String key : data.keySet()){
                stringBuilder.append(key).append("='").append(data.get(key)).append("', ");
            }
            stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
            stringBuilder.append(" where id='").append(entity.getId()).append("'");
            sql = stringBuilder.toString();
        }
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();

    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {

        Map<String, Object> map= new HashMap<>();

        for (Field field : getAllOpenFields(entity.getClass(), new ArrayList<Field>())){
            field.setAccessible(true);
            if (field.getType().equals(Boolean.class)){
                if (field.get(entity).toString().equals("true")){
                    map.put(field.getName(), 1);
                }else{
                    map.put(field.getName(), 0);
                }
            }else{
                map.put(field.getName(), field.get(entity));
            }
        }
        return map;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {

        ArrayList<T> list = new ArrayList<>();

        while (resultset.next()) {

            T object = clazz.newInstance();

            for (Field field : getAllOpenFields(clazz, new ArrayList<Field>())) {

                field.setAccessible(true);
                field.set(object, resultset.getObject(field.getName()));
            }
            list.add(object);
        }
        return list;
    }

   //get all open fields from all class hierarchy
    private <T extends Entity> ArrayList<Field> getAllOpenFields(Class<T> clazz, ArrayList<Field> list){
        ArrayList<Field> fields = list;
        for (Field field: clazz.getDeclaredFields()){
            if (!field.isAnnotationPresent(Ignore.class)) {
            fields.add(field);
            }
        }
        if (!clazz.isAnnotationPresent(PrimaryClass.class)){
            Class c = clazz.getSuperclass();
            fields = getAllOpenFields(c, fields);
        }
        return fields;
    }
}
