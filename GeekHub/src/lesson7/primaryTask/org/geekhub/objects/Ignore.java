package lesson7.primaryTask.org.geekhub.objects;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used by any {@link lesson7.primaryTask.org.geekhub.storage.Storage} implementation to identify fields
 * of {@link lesson7.primaryTask.org.geekhub.objects.Entity} that need to be avoided from being stored
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Ignore {
}
