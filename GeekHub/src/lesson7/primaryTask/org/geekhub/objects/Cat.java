package lesson7.primaryTask.org.geekhub.objects;

public class Cat extends Entity{

    @Ignore
    private static int counter = 1;

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "id = " +  getId() + " Name = " + getName() + " age = " + getAge();
    }

    @Override
    public int getNewId() {
        return counter++;
    }
}
