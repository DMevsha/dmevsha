package lesson7.primaryTask.org.geekhub.objects;

/**
 * Base class for all objects that could be stored with any {@link lesson7.primaryTask.org.geekhub.storage.Storage} implementation.
 */
@PrimaryClass
public abstract class Entity {


    /**
     * Identifier of the entity. Should be updated only by {@link lesson7.primaryTask.org.geekhub.storage.Storage} implementation.
     */
    protected Integer id;

    /**
     * Determines is object new or not based on its id.
     * @return true if object not yet saved, false otherwise.
     */
    public boolean isNew() {

        return getId() == null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public abstract int getNewId();
}
