package lesson7.secondaryTask.src;

import java.sql.*;


public class DBManager implements AutoCloseable {

    Connection connection;

    public DBManager(String hostName, String dbName, String login, String password) throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://" + hostName + "/" + dbName, login, password );
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

    public void executeQuery(String sql) throws SQLException {
        try (Statement stmt = connection.createStatement()) {

            ResultSet resultSet = stmt.executeQuery(sql);

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int counter = resultSetMetaData.getColumnCount();
            String underline = "";
            for (int i = 1; i <= counter; i++){
                System.out.print(celling(resultSetMetaData.getColumnName(i)));
                underline += "=============";
            }
            System.out.println("\n" + underline);
            while (resultSet.next()){
                for (int i = 1; i <= counter; i++){
                    System.out.print(celling(resultSet.getObject(i, resultSetMetaData.getColumnClassName(i).getClass())));
                }System.out.println();
            }

        }
    }

    private String celling(String s){
        StringBuilder stringBuilder = new StringBuilder();
        if (s.length() > 10){
            s = s.substring(0, 9);
        }
        stringBuilder.append("|<").append(s).append(">").append("          ");
        stringBuilder.delete(14, stringBuilder.length());

        return stringBuilder.toString();
    }

    public void updateExecuteUpdate(String command) throws SQLException {
        int i;
        try (Statement statement = connection.createStatement()){
            i = statement.executeUpdate(command);
        }
        System.out.format("The number of modified records is %d%n", i);
    }

}
